import java.util.*;

/**
 * Created by jurek on 12/15/16.
 */
public class Constants {
    static final String FAIR_STATE = "F";
    static final String LOADED_STATE = "L";
    static final Map<Integer, Double> FAIR_STATE_EMISSION_PROBABILITIES;
    static final Map<Integer, Double> LOADED_STATE_EMISSION_PROBABILITIES;

    static {
        Hashtable<Integer, Double> tmp = new Hashtable<>();
        for (int i = 1; i < 7; i++) {
            tmp.put(i, 1 / 6.0);
        }
        FAIR_STATE_EMISSION_PROBABILITIES = Collections.unmodifiableMap(tmp);

        tmp = new Hashtable<>();
        for (int i = 1; i < 6; i++) {
            tmp.put(i, 1 / 10.0);
        }
        tmp.put(6, 0.5);
        LOADED_STATE_EMISSION_PROBABILITIES = Collections.unmodifiableMap(tmp);
    }
}
