import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jurek on 12/15/16.
 */
public abstract class DiceHMM {
    protected double fromFairToLoadedProbability;
    protected double fromLoadedToFairProbability;


//    A'Posteriori algorithm fields
    protected List<Double> fairPrefixProbabibilties;
    protected List<Double> loadedPrefixProbabilities;

    protected List<Double> fairSufixProbabilities;
    protected List<Double> loadedSufixProbabilities;


//    A'Posteriori algorithm protected methods
    protected void initPrefixProbabilities(Integer initObservation){
        Double initFairPrefixProbability = Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(initObservation) * 0.5;
        Double initLoadedPrefixProbability = Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(initObservation) * 0.5;

        fairPrefixProbabibilties = new ArrayList<>();
        loadedPrefixProbabilities = new ArrayList<>();

        fairPrefixProbabibilties.add(initFairPrefixProbability);
        loadedPrefixProbabilities.add(initLoadedPrefixProbability);
    }

    protected void initSufixProbabilities(){
        fairSufixProbabilities = new ArrayList<>();
        loadedSufixProbabilities = new ArrayList<>();

        fairSufixProbabilities.add(1.0);
        loadedSufixProbabilities.add(1.0);
    }

    protected void calcPrefixProbabilities(List<Integer> observations){
        int observation;
        Double newFairPrefixProbability;
        Double newLoadedPrefixProbability;

        for (int i = 1; i < observations.size(); i++){
            observation = observations.get(i);

            newFairPrefixProbability = Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(observation) *
                    (fairPrefixProbabibilties.get(i-1) * (1.0 - fromFairToLoadedProbability) + loadedPrefixProbabilities.get(i-1) * fromLoadedToFairProbability);
            newLoadedPrefixProbability = Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(observation) *
                    (loadedPrefixProbabilities.get(i-1) * (1.0 - fromLoadedToFairProbability) + fairPrefixProbabibilties.get(i-1) * fromFairToLoadedProbability);


            fairPrefixProbabibilties.add(newFairPrefixProbability);
            loadedPrefixProbabilities.add(newLoadedPrefixProbability);


        }
    }

    protected void calcSufixProbabilities(List<Integer> observations){
        int observation;
        Double newFairSufixProbability;
        Double newLoadedSufixProbability;
        Double lastFairSufixProbability;
        Double lastLoadedSufixProbability;

        for (int i = observations.size() - 1; i > 0; i--){
            observation = observations.get(i);

            lastFairSufixProbability = fairSufixProbabilities.get(fairSufixProbabilities.size() - 1);
            lastLoadedSufixProbability = loadedSufixProbabilities.get(loadedSufixProbabilities.size() -1);

            newFairSufixProbability = (1.0 - fromFairToLoadedProbability) *
                    Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(observation) *
                    lastFairSufixProbability +
                    fromFairToLoadedProbability *
                    Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(observation) *
                    lastLoadedSufixProbability;
            newLoadedSufixProbability = (1.0 - fromLoadedToFairProbability) *
                    Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(observation) *
                    lastLoadedSufixProbability +
                    fromLoadedToFairProbability *
                    Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(observation) *
                    lastFairSufixProbability;

            fairSufixProbabilities.add(newFairSufixProbability);
            loadedSufixProbabilities.add(newLoadedSufixProbability);
        }

        Collections.reverse(fairSufixProbabilities);
        Collections.reverse(loadedSufixProbabilities);
    }

    protected List<String> getFinalPredictionAPosteriori(){
        List<String> finalPrediction = new ArrayList<>();
        for (int i = 0; i < fairPrefixProbabibilties.size(); i++){
            if (fairPrefixProbabibilties.get(i) * fairSufixProbabilities.get(i) >
                    loadedPrefixProbabilities.get(i) * loadedSufixProbabilities.get(i)){
                finalPrediction.add(Constants.FAIR_STATE);
            } else {
                finalPrediction.add(Constants.LOADED_STATE);
            }
        }
        return finalPrediction;
    }
}
