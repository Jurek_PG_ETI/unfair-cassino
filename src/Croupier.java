import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by jurek on 12/15/16.
 */
public class Croupier {
    private double fromFairToLoadedProbability;
    private double fromLoadedToFairProbability;
    private String currentDice;

    public Croupier(double fromFairToLoadedProbability, double fromLoadedToFairProbability) {
        this.fromFairToLoadedProbability = fromFairToLoadedProbability;
        this.fromLoadedToFairProbability = fromLoadedToFairProbability;
    }

    public void startExperiment(int rollOfDiceCount){
        List<Integer> rolls = new ArrayList<>(rollOfDiceCount);
        List<String> diceStates = new ArrayList<>(rollOfDiceCount);

        rollDice(rollOfDiceCount, rolls, diceStates);

        DiceHMMPredictor diceHMMPredictor = new DiceHMMPredictor(fromFairToLoadedProbability, fromLoadedToFairProbability);

        List<String> predictedStatesViterbi = diceHMMPredictor.predictStatesViterbi(rolls);
        List<String> predictedStatesAPosteriori = diceHMMPredictor.predictStatesAPosteriori(rolls);

        printExperimentResult(rolls, diceStates, predictedStatesViterbi, predictedStatesAPosteriori);
    }

    public void testHMM(){
        DiceHMMPredictor diceHMMPredictor = new DiceHMMPredictor(fromFairToLoadedProbability, fromLoadedToFairProbability);

        List<Integer> testRolls = new ArrayList<>();
        List<String> testDice = new ArrayList<>();
        testDice.addAll(Arrays.asList("F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F"));
        testRolls.addAll(Arrays.asList(3, 1, 5, 1, 1, 6, 2, 4, 6, 4, 4, 6, 6, 4, 4, 2, 4, 5, 3, 1, 1, 3, 2, 1, 6, 3, 1, 1, 6, 4, 1, 5, 2, 1, 3, 3, 6, 2, 5, 1, 4, 4, 5, 4, 3));
        testRolls.addAll(Arrays.asList(6, 3, 1, 6, 5, 6, 6, 2, 6, 5, 6, 6, 6, 6, 6, 6, 5, 1, 1, 6, 6));
        testDice.addAll(Arrays.asList("L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L"));

        testRolls.addAll(Arrays.asList(4, 5, 3, 1, 3, 2, 6, 5, 1, 2, 4, 5));
        testDice.addAll(Arrays.asList("F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F"));

        testRolls.addAll(Arrays.asList(6, 3, 6, 6, 6, 4, 6, 3, 1, 6, 3, 6, 6, 6, 3, 1, 6, 2, 3));
        testDice.addAll(Arrays.asList("L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "F", "F", "F"));

        testRolls.addAll(Arrays.asList(2, 6, 4, 5, 5, 2, 3, 6, 2, 6, 6, 6, 6, 6));
        testDice.addAll(Arrays.asList("L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L", "L"));

        testRolls.addAll(Arrays.asList(6, 2, 5, 1, 5, 1, 6, 3, 1));
        testDice.addAll(Arrays.asList("F", "F", "F", "F", "F", "F", "F", "F", "F"));

        List<String> predictedStatesViterbi = diceHMMPredictor.predictStatesViterbi(testRolls);
        List<String> predictedStateAPosteriori = diceHMMPredictor.predictStatesAPosteriori(testRolls);

        printExperimentResult(testRolls, testDice, predictedStatesViterbi, predictedStateAPosteriori);
    }

    public void testBaumWelch() {
        DiceHMMTrainer diceHMMTrainer = new DiceHMMTrainer();
        List<List<Integer>> observationsSequences = getObservationsSequencesForBaumWelch(100, 300);
        DiceHMMTrainer.Probabilities estimatedProbabilities = diceHMMTrainer.trainBaumWelsch(observationsSequences);
        printBaumWelchTrainingResults(estimatedProbabilities);
    }

    private void rollDice(int rollOfDiceCount, List<Integer> rolls, List<String> diceStates){
        pickDice();
        boolean diceSwitch;

        for(int i = 0; i < rollOfDiceCount; i++) {
            diceSwitch = changeDice();
            if(diceSwitch && currentDice.equals(Constants.FAIR_STATE)) {
                currentDice = Constants.LOADED_STATE;
                rolls.add(loadedRollOfDice());
            }
            else if(diceSwitch && currentDice.equals(Constants.LOADED_STATE)) {
                currentDice = Constants.FAIR_STATE;
                rolls.add(fairRollOfDice());
            }
            else if(currentDice.equals(Constants.FAIR_STATE)) {
                rolls.add(fairRollOfDice());
            }
            else {
                rolls.add(loadedRollOfDice());
            }
            diceStates.add(currentDice);
        }
    }

    private List<List<Integer>> getObservationsSequencesForBaumWelch(int sequencesCount, int rollsInSequenceCount){
        List<List<Integer>> observationsSequences = new ArrayList<>(sequencesCount);
        for(int i = 0; i < sequencesCount; i++){
            List<Integer> rolls = new ArrayList<>(rollsInSequenceCount);
            List<String> diceStates = new ArrayList<>(rollsInSequenceCount);

            rollDice(rollsInSequenceCount, rolls, diceStates);

            observationsSequences.add(rolls);
        }

        return observationsSequences;

    }

    private int fairRollOfDice() {
        return (int)(Math.random() * 6) + 1;
    }

    private int loadedRollOfDice() {
        int [] lowWeightNumbers = {1, 2, 3, 4, 5};
        int highWeightNumber = 6;
        int threshold = 50;

        int spot = (int) (Math.random()*100);
        if(spot < threshold) {
            return lowWeightNumbers[(int) (Math.random()*lowWeightNumbers.length)];
        }
        else {
            return highWeightNumber;
        }
    }

    private boolean changeDice() {
        double spot = Math.random();
        double changeThreshold;

        if(currentDice.equals(Constants.FAIR_STATE)) {
            changeThreshold = fromFairToLoadedProbability;
        }
        else {
            changeThreshold = fromLoadedToFairProbability;
        }

        return spot < changeThreshold;
    }

    private void pickDice() {
        if(Math.random() > 0.5) {
            currentDice = Constants.FAIR_STATE;
        }
        else {
            currentDice = Constants.LOADED_STATE;
        }
    }

    private <T> void printList(List<T> list) {
        for(T element : list) {
            System.out.print(element + " ");
        }
    }

    private void printExperimentResult(List<Integer> diceRolls, List<String> dice, List<String> viterbiPrediction, List<String> aPosterioriPrediction){
        System.out.print("\nRolls        ");
        printList(diceRolls);
        System.out.print("\nDie          ");
        printList(dice);
        System.out.print("\nViterbi      ");
        printList(viterbiPrediction);
        System.out.print("\nA'Posteriori ");
        printList(aPosterioriPrediction);
        System.out.println();
        System.out.println();
        System.out.println(String.format("Viterbi accuracy:      %.2f%s", getPredictionAccuracy(dice, viterbiPrediction), "%."));
        System.out.println(String.format("A'Posteriori accuracy: %.2f%s", getPredictionAccuracy(dice, aPosterioriPrediction), "%."));
    }

    private float getPredictionAccuracy(List<String> trueStates, List<String> predictedStates){
        int goodPredictionsCount = 0;
        int statesCount = trueStates.size();
        for (int i = 0; i < trueStates.size(); i++){
            if (trueStates.get(i).equals(predictedStates.get(i))){
                goodPredictionsCount++;
            }
        }
        return (100F * goodPredictionsCount) / statesCount;
    }

    private void printBaumWelchTrainingResults(DiceHMMTrainer.Probabilities estimatedProbabilities){
        System.out.println("       Parameter       |   Real value   | Estimated value");
        System.out.println("-----------------------|----------------|----------------");

        System.out.println(String.format("F->L change prob       |    %.6f    |    %.6f    ",
                fromFairToLoadedProbability, estimatedProbabilities.getFromFairToLoadedProbability()));
        System.out.println("-----------------------|----------------|----------------");
        System.out.println(String.format("L->F change prob       |    %.6f    |    %.6f    ",
                fromLoadedToFairProbability, estimatedProbabilities.getFromLoadedToFairProbability()));
        System.out.println("-----------------------|----------------|----------------");
        printEmissionProbabilitiesComparison("F state emission probs |",
                estimatedProbabilities.getFairStateEmissionProbabilities(), Constants.FAIR_STATE);
        System.out.println("-----------------------|----------------|----------------");
        printEmissionProbabilitiesComparison("L state emission probs |",
                estimatedProbabilities.getLoadedStateEmissionProbabilities(), Constants.LOADED_STATE);
    }

    private void printEmissionProbabilitiesComparison(String header, Map<Integer, Double> estimatedEmissionProbabilities,
                                                      String state){
        Map<Integer, Double> realEmissionProbabilities;
        if(state.equals(Constants.FAIR_STATE)){
            realEmissionProbabilities = Constants.FAIR_STATE_EMISSION_PROBABILITIES;
        } else if (state.equals(Constants.LOADED_STATE)){
            realEmissionProbabilities = Constants.LOADED_STATE_EMISSION_PROBABILITIES;
        } else {
            return;
        }

        System.out.println(String.format("%s    %.6f    |    %.6f    ", header,
                realEmissionProbabilities.get(1), estimatedEmissionProbabilities.get(1)));

        for(int i = 2; i < 7; i++){
            System.out.println(String.format("                       |    %.6f    |    %.6f    ",
                    realEmissionProbabilities.get(i), estimatedEmissionProbabilities.get(i)));
        }

    }

}
