import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jurek on 1/15/17.
 */
public class DiceHMMPredictor extends  DiceHMM {
    private Double currentFairStateProbability;
    private Double currentLoadedStateProbability;

    private List<String> fairPath;
    private List<String> loadedPath;

    public DiceHMMPredictor(double fromFairToLoadedProbability, double fromLoadedToFairProbability) {
        
        this.fromFairToLoadedProbability = fromFairToLoadedProbability;
        this.fromLoadedToFairProbability = fromLoadedToFairProbability;
    }

    public List<String> predictStatesViterbi(List<Integer> observations){
        initStateProbabilitesAndPaths(observations.get(0));
        calcStateProbabilitesAndPaths(observations);
        return getFinalPredictionViterbi();
    }

    public List<String> predictStatesAPosteriori(List<Integer> observations){
        initPrefixProbabilities(observations.get(0));
        calcPrefixProbabilities(observations);

        initSufixProbabilities();
        calcSufixProbabilities(observations);

        return getFinalPredictionAPosteriori();
    }

    //    Viterbi algorithm private methods
    private void initStateProbabilitesAndPaths(int initObservation){
        currentFairStateProbability = Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(initObservation) * 0.5;
        currentLoadedStateProbability = Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(initObservation) * 0.5;

        fairPath = new ArrayList<>();
        loadedPath = new ArrayList<>();
    }

    private void calcStateProbabilitesAndPaths(List<Integer> observations){
        for(int i = 1; i < observations.size(); i++){
            updateStateProbabilitiesAndPath(observations.get(i));
        }

        fairPath.add(Constants.FAIR_STATE);
        loadedPath.add(Constants.LOADED_STATE);
    }

    private void updateStateProbabilitiesAndPath(int observation){
        Double newFairStateProbability = Constants.FAIR_STATE_EMISSION_PROBABILITIES.get(observation);
        Double newLoadedStateProbability = Constants.LOADED_STATE_EMISSION_PROBABILITIES.get(observation);

        Double fromFairToFairInIthStepProbability = currentFairStateProbability * (1.0 - fromFairToLoadedProbability);
        Double fromLoadedToFairInIthStepProbability = currentLoadedStateProbability * fromLoadedToFairProbability;

        if(fromLoadedToFairInIthStepProbability > fromFairToFairInIthStepProbability){
            newFairStateProbability *= fromLoadedToFairInIthStepProbability;
            fairPath.add(Constants.LOADED_STATE);
        } else {
            newFairStateProbability *= fromFairToFairInIthStepProbability;
            fairPath.add(Constants.FAIR_STATE);
        }

        Double fromLoadedToLoadedInIthStepProbability = currentLoadedStateProbability * (1.0 - fromLoadedToFairProbability);
        Double fromFairToLoadedInIthStepProbability = currentFairStateProbability * fromFairToLoadedProbability;

        if(fromFairToLoadedInIthStepProbability > fromLoadedToLoadedInIthStepProbability){
            newLoadedStateProbability *= fromFairToLoadedInIthStepProbability;
            loadedPath.add(Constants.FAIR_STATE);
        } else {
            newLoadedStateProbability *= fromLoadedToLoadedInIthStepProbability;
            loadedPath.add(Constants.LOADED_STATE);
        }

        currentFairStateProbability = newFairStateProbability;
        currentLoadedStateProbability = newLoadedStateProbability;
    }

    private List<String> getFinalPredictionViterbi(){
        int finalPredictionSize = fairPath.size();
        String[] finalPrediction = new String [finalPredictionSize];

        int last = finalPredictionSize - 1;
        if (currentFairStateProbability > currentLoadedStateProbability){
            finalPrediction[last] = fairPath.get(last);
        } else {
            finalPrediction[last] = loadedPath.get(last);
        }

        for (int i = last; i > 0; i--){
            if (finalPrediction[i].equals(Constants.FAIR_STATE)){
                finalPrediction[i-1] = fairPath.get(i-1);
            } else {
                finalPrediction[i-1] = loadedPath.get(i-1);
            }
        }

        return Arrays.asList(finalPrediction);
    }
}
