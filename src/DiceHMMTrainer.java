import java.util.*;

/**
 * Created by jurek on 1/15/17.
 */
public class DiceHMMTrainer extends DiceHMM {
    private Map<Integer, Double> fairStateEmissionProbabilities;
    private Map<Integer, Double> loadedStateEmissionProbabilities;

    private double fromFairToLoadedChangeCount;
    private double fromFairToFairChangeCount;
    private double fromLoadedToFairChangeCount;
    private double fromLoadedToLoadedChangeCount;

    private Map<Integer, Double> fairStateEmissionCounts;
    private Map<Integer, Double> loadedStateEmissionCounts;

    public DiceHMMTrainer() {
        initProbabilities();
    }

    public Probabilities trainBaumWelsch(List<List<Integer>> observationsSequences){
        for(int i = 0; i < 1000; i++){ // TODO Change fixed iterations to stop criterion
            initCounts();

            for(List<Integer> observations : observationsSequences){
                initPrefixProbabilities(observations.get(0));
                calcPrefixProbabilities(observations);

                initSufixProbabilities();
                calcSufixProbabilities(observations);

                double observationProbability = fairPrefixProbabibilties.get(fairPrefixProbabibilties.size() - 1) +
                        loadedPrefixProbabilities.get(loadedPrefixProbabilities.size() -1);

                updateChangeCounts(observations, observationProbability);
                updateEmissionCounts(observations, observationProbability);
            }

            updateChangeProbabilities();
            updateEmissionProbabilities();

        }

        return new Probabilities(fromFairToLoadedProbability, fromLoadedToFairProbability,
                fairStateEmissionProbabilities, loadedStateEmissionProbabilities);
    }

    private void initProbabilities(){
        initChangeProbabilities();

        fairStateEmissionProbabilities = new HashMap<>();
        loadedStateEmissionProbabilities = new HashMap<>();

        initEmissionProbabilities(fairStateEmissionProbabilities);
        initEmissionProbabilities(loadedStateEmissionProbabilities);
    }

    private void initChangeProbabilities(){
        fromLoadedToFairProbability = Math.random();
        fromFairToLoadedProbability = Math.random();
    }

    private void initEmissionProbabilities(Map<Integer, Double> emissionProbabilities){
        List<Double> emissionProbabilitiesList = new ArrayList<>();
        double emissionProbabilitiesSum = 0;
        for(int i = 0; i < 6; i++){
            double newEmissionProbability = Math.random();
            emissionProbabilitiesList.add(newEmissionProbability);
            emissionProbabilitiesSum += newEmissionProbability;
        }

        emissionProbabilities.clear();
        for(int i = 1; i < 7; i++) {
            emissionProbabilities.put(i, emissionProbabilitiesList.get(i - 1) / emissionProbabilitiesSum);
        }
    }

    private void initCounts(){
        fromFairToLoadedChangeCount = 0;
        fromFairToFairChangeCount = 0;
        fromLoadedToFairChangeCount = 0;
        fromLoadedToLoadedChangeCount = 0;

        fairStateEmissionCounts = new HashMap<>();
        loadedStateEmissionCounts = new HashMap<>();
        for(int i = 1; i < 7; i++){
            fairStateEmissionCounts.put(i, 0.0);
            loadedStateEmissionCounts.put(i, 0.0);
        }
    }

    private void updateChangeCounts(List<Integer> observations, double observationProbability){
        double fromFairToLoadedSumProbability = 0.0;
        double fromFairToFairSumProbability = 0.0;
        double fromLoadedToFairSumProbability = 0.0;
        double fromLoadedToLoadedSumProbability = 0.0;

        for(int j = 0; j < observations.size() - 1; j++){
            fromFairToLoadedSumProbability +=  fairPrefixProbabibilties.get(j) * fromFairToLoadedProbability
                    * loadedStateEmissionProbabilities.get(observations.get(j+1)) * loadedSufixProbabilities.get(j+1);

            fromFairToFairSumProbability += fairPrefixProbabibilties.get(j) * (1 - fromFairToLoadedProbability)
                    * fairStateEmissionProbabilities.get(observations.get(j+1)) * fairSufixProbabilities.get(j+1);

            fromLoadedToFairSumProbability += loadedPrefixProbabilities.get(j) * fromLoadedToFairProbability
                    * fairStateEmissionProbabilities.get(observations.get(j+1)) * fairSufixProbabilities.get(j+1);

            fromLoadedToLoadedSumProbability += loadedPrefixProbabilities.get(j) * (1 - fromLoadedToFairProbability)
                    * loadedStateEmissionProbabilities.get(observations.get(j+1)) * loadedSufixProbabilities.get(j+1);
        }

        fromFairToLoadedChangeCount += fromFairToLoadedSumProbability / observationProbability;
        fromFairToFairChangeCount += fromFairToFairSumProbability / observationProbability;
        fromLoadedToFairChangeCount += fromLoadedToFairSumProbability / observationProbability;
        fromLoadedToLoadedChangeCount += fromLoadedToLoadedSumProbability / observationProbability;
    }

    private void updateEmissionCounts(List<Integer> observations, double observationProbability){
        Map<Integer, Double> fairStateEmissionSumProbabilities = new HashMap<>();
        Map<Integer, Double> loadedStateEmissionSumProbabilities = new HashMap<>();

        for(int i = 1; i < 7; i++){
            fairStateEmissionSumProbabilities.put(i, 0.0);
            loadedStateEmissionSumProbabilities.put(i, 0.0);
        }

        for(int i = 0; i < observations.size(); i++){
            for(int j = 1; j < 7; j++){
                if(observations.get(i) != j){
                    continue;
                }

                fairStateEmissionSumProbabilities.put(j, fairStateEmissionSumProbabilities.get(j) +
                    fairPrefixProbabibilties.get(i) * fairSufixProbabilities.get(i));

                loadedStateEmissionSumProbabilities.put(j, loadedStateEmissionSumProbabilities.get(j) +
                    loadedPrefixProbabilities.get(i) * loadedSufixProbabilities.get(i));
            }
        }

        for(int i = 1; i < 7; i++){
            fairStateEmissionCounts.put(i, fairStateEmissionSumProbabilities.get(i) / observationProbability);
            loadedStateEmissionCounts.put(i, loadedStateEmissionSumProbabilities.get(i) / observationProbability);
        }
    }

    private void updateChangeProbabilities(){
        fromFairToLoadedProbability = fromFairToLoadedChangeCount / (fromFairToLoadedChangeCount + fromFairToFairChangeCount);
        fromLoadedToFairProbability = fromLoadedToFairChangeCount / (fromLoadedToFairChangeCount + fromLoadedToLoadedChangeCount);
        return;
    }

    private void updateEmissionProbabilities(){
        double fairStateEmissionsCountSum = 0.0;
        double loadedStateEmissionsCountSum = 0.0;

        for(int i = 1; i < 7; i++){
            fairStateEmissionsCountSum += fairStateEmissionCounts.get(i);
            loadedStateEmissionsCountSum += loadedStateEmissionCounts.get(i);
        }

        for(int i = 1; i < 7; i++){
            fairStateEmissionProbabilities.put(i, fairStateEmissionCounts.get(i) / fairStateEmissionsCountSum);
            loadedStateEmissionProbabilities.put(i, loadedStateEmissionCounts.get(i) / loadedStateEmissionsCountSum);
        }
    }

    class Probabilities{
        private double fromFairToLoadedProbability;
        private double fromLoadedToFairProbability;

        private Map<Integer, Double> fairStateEmissionProbabilities;
        private Map<Integer, Double> loadedStateEmissionProbabilities;

        public Probabilities(double fromFairToLoadedProbability, double fromLoadedToFairProbability,
                             Map<Integer, Double> fairStateEmissionProbabilities,
                             Map<Integer, Double> loadedStateEmissionProbabilities) {
            this.fromFairToLoadedProbability = fromFairToLoadedProbability;
            this.fromLoadedToFairProbability = fromLoadedToFairProbability;
            this.fairStateEmissionProbabilities = fairStateEmissionProbabilities;
            this.loadedStateEmissionProbabilities = loadedStateEmissionProbabilities;
        }

        public double getFromFairToLoadedProbability() {
            return fromFairToLoadedProbability;
        }

        public double getFromLoadedToFairProbability() {
            return fromLoadedToFairProbability;
        }

        public Map<Integer, Double> getFairStateEmissionProbabilities() {
            return fairStateEmissionProbabilities;
        }

        public Map<Integer, Double> getLoadedStateEmissionProbabilities() {
            return loadedStateEmissionProbabilities;
        }
    }
}
