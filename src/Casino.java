import java.util.Scanner;

/**
 * Created by jurek on 12/15/16.
 */
public class Casino {
    public static void main(String[] args){
//        casinoExperimentTest();
//        casinoExperiment();
        casinoBaumWelchTest();
    }

    private static void casinoExperiment(){
        double fromFairToLoadedProbability, fromLoadedToFairProbability;
        int rollOfTheDiceCount;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Dice rolls count: ");
        rollOfTheDiceCount = scanner.nextInt();

        System.out.print("Probability to change from fair state to loaded: ");
        fromFairToLoadedProbability = scanner.nextDouble();

        System.out.print("Probability to change from loaded state to fair: ");
        fromLoadedToFairProbability = scanner.nextDouble();

        Croupier croupier = new Croupier(fromFairToLoadedProbability, fromLoadedToFairProbability);
        croupier.startExperiment(rollOfTheDiceCount);
    }

    private static void casinoExperimentTest(){
        Croupier croupier = new Croupier(0.05, 0.1);
        croupier.testHMM();
    }

    private static void casinoBaumWelchTest(){
        Croupier croupier = new Croupier(0.05, 0.1);
        croupier.testBaumWelch();
    }
}
